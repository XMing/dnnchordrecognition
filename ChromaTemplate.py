# -*- coding: utf-8 -*-
"""
Created on Wed Jun 10 23:32:21 2015

@author: xming
"""

import numpy as np
import ChordVocaburary as voc

tempBinary = [[1,0,0,0,1,0,0,1,0,0,0,0],[1,0,0,1,0,0,0,1,0,0,0,0]]
noChord = [0.083,0.083,0.083,0.083,0.083,0.083,0.083,0.083,0.083,0.083,0.083,0.083]

tempBinaryMatrix = np.zeros((voc.TotalChordType,12))

for i in range(voc.TotalChordType):
    if i==voc.TotalChordType-1:
        tempBinaryMatrix[i,:] = noChord
    else:
        tone = i%12
        ctype = i/12
        tempBinaryMatrix[i,:] = np.roll(tempBinary[ctype],tone)
        

def shift(arr,n): 
    ret = list(arr)
    for i in range(n):
        ret.insert(0,ret.pop())
    return ret

def getTemplateBinary():
    temp = []
    for i in range(12):
        item = []
        for j in range(2):
            item.append(shift(tempBinary[j],i))
        temp.append(item)
    return temp

def getTemplateMatrix(harmonized=False):
    matrix = np.array(tempBinary[0])
    for chord in range(voc.TotalChordType):
        if harmonized:
            matrix = np.vstack((matrix,getHarmonizedTemplate(chord)))
        else:
            matrix = np.vstack((matrix,getTemplate(chord)))
            
    matrix = matrix[1:] #delete first row
    return np.matrix(matrix)

    
def getTemplate(n):
    return tempBinaryMatrix[n,:]
    
def getHarmonizedTemplate(n,harmonics=4):
    if n==24:
        return noChord
    binaryTemp = getTemplate(n)
    harmonyTemp = np.zeros(12)
    for i in range(12):
        if binaryTemp[i]==0:
            continue
        harmonyTemp[i] += 1
        for h in range(2,harmonics+1):
            position = i * h % 12
            harmonyTemp[position] += 0.6**(h-1)
    harmonyTemp = harmonyTemp / np.sum(harmonyTemp)        
    return harmonyTemp