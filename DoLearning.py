# -*- coding: utf-8 -*-
"""
Created on Thu Mar  3 16:05:25 2016

@author: wuyiming
"""


import utility as util
import DNNAcousticModel as dnn
import numpy as np
from chainer import serializers

#audio_path="Beatles_Data/Audio/"
label_path="Beatles_Data/chordlab/The Beatles/"
cqt_path = "Beatles_Data/CQT/"



#audiolist = util.GetFileList(audio_path)
labellist = util.GetFileList(label_path)
cqtlist = util.GetFileList(cqt_path)

#print "Training Language Model..."
#train_sequences = util.ConstructLangTrainData(labellist[20:])
#langmodel = rnn.TrainLangModel(train_sequences,epoch_cnt=200)

"""
DNN学習とコード遷移確率統計　（学習用データは事前に計算済み）
"""
print "Parsing Data..."
X,Y = util.ConstructTrainData(cqtlist,labellist)
transmat = util.GetTransMatrix(Y)
print "Start DNN Training..."
dnnmodel = dnn.TrainDiscriminativeDNNAcousticModel(X,Y,epoch1=30,epoch2=30,gpu=True)
serializers.save_npz("dnn.model",dnnmodel)
np.save("transmat.npy",transmat)
print "Finished!"