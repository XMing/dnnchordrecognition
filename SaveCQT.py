# -*- coding: utf-8 -*-
"""
Created on Tue Nov 17 22:09:29 2015

@author: xming
"""
from librosa.core import load,cqt,pseudo_cqt
from librosa.effects import harmonic
import numpy as np
from os import listdir

audio_path="Beatles_Data/Audio/"
albums = sorted(listdir(audio_path))
cqt_path="Beatles_Data/CQT/"
i = 0
for alb in albums:
    filelist = sorted(listdir(audio_path+alb))
    for f in filelist:
        i += 1
        print "Processing file %d/179" % i
        file_path = audio_path+alb+"/"+f
        y,sr = load(file_path,sr=44100)
        spectrum = np.transpose(np.abs(cqt(harmonic(y),sr=sr,hop_length=1024,bins_per_octave=24,n_bins=168,real=False,filter_scale=2)))
        cqtfile_path = cqt_path+alb+"/"+f+".npy"
        np.save(cqtfile_path,spectrum)