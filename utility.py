# -*- coding: utf-8 -*-
"""
Created on Tue Nov 17 16:11:16 2015

@author: xming
"""

from os import listdir
import numpy as np

def GetFileList(path):
    filelist = []    
    albumlist = sorted(listdir(path))
    for alb in albumlist:
        f_path = path+alb+"/"
        flist = sorted(listdir(f_path))
        for f in flist:
            filelist.append(f_path+f)
            
    return filelist
    
def ConstructTrainData(cqt_list,labelfile_list,H=1024):
    itemcnt = len(cqt_list)
    X = np.zeros(168)
    Y = np.array([])
    for i in xrange(itemcnt):
        #print "Loading: %d/%d" % (i+1,itemcnt)
        spectrum = np.load(cqt_list[i])
        
        labels = ParseLabelFile(labelfile_list[i],H=H)
        l = min([np.size(spectrum,axis=0),np.size(labels)])
        #legalidx = np.where(labels[:l]>=0)
        X = np.vstack((X,spectrum[:l]))
        Y = np.append(Y,labels[:l])
        
    return X[1:,:].astype("float32"),Y.astype("int32")
def ConstructDataPairs(cqt_list,labelfile_list,H=1024):
    itemcnt = len(cqt_list)
    cqt = []
    lab = []
    for i in xrange(itemcnt):
        spectrum = np.load(cqt_list[i]).astype("float32")
        labels = ParseLabelFile(labelfile_list[i],H=H).astype("int32")
        l = min([np.size(spectrum,axis=0),np.size(labels)])
        cqt.append(spectrum[:l,:])
        lab.append(labels[:l])
        
    return cqt,lab
def ConstructLangTrainData(labelfile_list,H=1024):
    itemcnt = len(labelfile_list)
    ret = []
    for i in xrange(itemcnt):
        labels = ParseLabelFile(labelfile_list[i],H=H)
        ret.append(labels)
    return ret
    
import ChordVocaburary as voc
def ParseBeatFile(filepath):
    f = file(filepath)
    line = f.readline()
    beat=[]
    while line!="":
        items = line.split()
        beat.append(float(items[0]))
        line = f.readline()
    return np.array(beat)
def ParseLabelFile(filepath,H=1024):
    f = file(filepath)
    line = f.readline()
    endtimelist = []
    labellist = []
    while line != "":
        items = line.split()
        endtime = float(items[1])
        label = voc.GetChordIDSign(items[2])
        endtimelist.append(endtime)
        labellist.append(label)
        line = f.readline()
    framecnt = np.round(endtimelist[len(endtimelist)-1]*44100/H)
    labelframe = np.zeros(framecnt,dtype="int32")
    for i in xrange(len(labellist)):
        start = 0 if i==0 else endtimelist[i-1]*44100/H
        start = int(np.round(start))
        end = int(np.round(endtimelist[i]*44100/H))
        labelframe[start:end] = labellist[i]
    
    return labelframe

def GetTransMatrix(labelframe):
    mat = np.zeros((25,25))
    T = np.size(labelframe)
    for t in range(T-1):
        mat[labelframe[t],labelframe[t+1]] += 1.0
        
    for i in range(25):
        mat[i,:] /= np.sum(mat[i,:])
    
    return mat
def Lowpass(X,alpha=0.75):
    T = np.size(X,axis=0)
    Y = X*alpha
    for i in xrange(1,T):
	Y[i,:] = Y[i,:] + (1-alpha)*Y[i-1,:]
    return np.array(Y,dtype="float32")

from scipy.signal import medfilt
def Medianfilter(X,size=5):
    return np.array(medfilt(X,[size,1]),dtype="float32")

def ZCAWhitening(X):
    N = X.shape[0]
    C = np.dot(X.T,X) / N
    U,lam,V = np.linalg.svd(C)
    eps = 0
    sqlam = np.sqrt(lam+eps)
    Uzca = np.dot(U / sqlam[np.newaxis,:],U.T)
    Z = np.dot(X,Uzca.T)
    return np.array(Z,dtype="float32")
