# -*- coding: utf-8 -*-
"""
Created on Mon Jun 15 13:19:35 2015

@author: xming
"""

ToneID = {'C':0,
          'C#':1,
          'Db':1,
          'D':2,
          'D#':3,
          'Eb':3,
          'E':4,
          'F':5,
          'F#':6,
          'Gb':6,
          'G':7,
          'G#':8,
          'Ab':8,
          'A':9,
          'A#':10,
          'Bb':10,
          'B':11,
          }
TypeID = {'':0,
          'maj':0,
          'maj(*1)':0,
          'maj(2)':0,
          'maj(*3)':0,
          'maj(4)':0,
          'maj(#4)':0,
          'maj(*5)':0,
          'min':1,
          'min(2)':1,
          'min(*3)':1,
          'min(*5)':1,
          'min(*b3)':1,
          'min(4)':1,
          '7':0,
          '7(*5,13)':0,
          '7(13)':0,
          'b7':0,
          'maj7':0,
          'maj7(*b5)':0,
          'maj7(*5)':0,
          'min7':1,
          'min7(b5)':1,
          'min7(*5)':1,
          'maj6':0,
          'maj6(9)':0,
          'maj9':0,
          'maj9(*7)':0,
          'maj(9)':0,
          'maj(b9)':0,
          'maj(11)':0,
          'maj(#11)':0,
          'min6':1,
          'min7(4)':1,
          'min7(*5,b6)':1,
          'min7(*b3)':1,
          'min7(2,*b3,4)':1,
          'min9':1,
          'min(9)':1,
          '9':0,
          '2':0,
          '3':0,
          'b3':0,
          '4':0,
          '#4':0,
          '5':0,
          '6':0,
          'b6':0,
          '9(11)':0,
          '9(*3)':0,
          '9(*3,11)':0,
          'aug':-1,
          'minmaj7':1,
          'dim7':-1,
          'hdim7':-1,
          'dim':-1,
          'aug(9,11)':-1,
          '7(#9)':0,
          '7(b9)':0,
          'sus4':-1,
          'sus4(9)':-1,
          'sus2':-1,
          'sus2(b7)':-1,
          'sus4(b7)':-1,
          'sus4(2)':-1,
          '(1)':-1,
          '(6)':-1,
          '(7)':-1,
          '(1,4,b7)':-1,
          '(1,2,4)':-1,
          '(1,4,b5)':-1,
          '(1,2,5,b6)':-1,
          '(1,5)':-1,
          '(1,b7)':-1,
          '(1,b3)':-1,
          '(1,b3,4)':-1,
          '(1,4)':-1,
          '(b3,5)':-1
          }
ToneChr = ['C','Db','D','Eb','E','F','Gb','G','Ab','A','Bb','B']

ChordType = ['','min']

TotalChordType = 24+1

def GetChordSignature(n):
    if n == len(ToneChr)*len(ChordType):
        return 'N'
    tone = ToneChr[n%12]
    chordtype = ChordType[n/12]
    return tone+':'+chordtype
    
import re
def GetChordID(tone,ctype):
    if tone<0 or ctype<0:
        return -1
    return ctype*12+tone
    
def GetChordIDSign(sign):
    if sign == 'N':
        return len(ToneChr)*len(ChordType)
    #root = re.findall('^([A-Z][#b]|[A-Z])',sign)
    #bass = re.findall('(?<=/).',sign)
    #ctype = re.findall('(?<=:).*[(/]',sign)
    #bracket = re.findall('\(.*\)',sign)
    #rootID = ToneID[root[0]]
        
    splitted = re.split('[:/]',sign)
    rootID = ToneID[splitted[0]]
    if len(splitted) == 1:
        ctypeID = 0
    else:
        ctypeID = TypeID[splitted[1]]
        
    return GetChordID(rootID,ctypeID)
    