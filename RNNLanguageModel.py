# -*- coding: utf-8 -*-
"""
Created on Thu Dec  3 21:25:50 2015

@author: xming
"""
from chainer import Variable,optimizers,Chain
import chainer.links as L
import chainer.functions as F
import numpy as np

nb_classes = 25

xp = np

class RNNLangModel(Chain):
    def __init__(self,train=True):
        super(RNNLangModel,self).__init__(
            embed=L.EmbedID(25,100),
            mid=L.LSTM(100,100),
            out=L.Linear(100,25))
            
        self.train = train
    def reset_state(self):
        self.mid.reset_state()
        
    def __call__(self,cur_word):
        x = self.embed(cur_word)
        h = F.dropout(self.mid(x),train=self.train)
        y = self.out(h)
        return y
    def ActivateSeq(self,seq):
        self.reset_state()
        for w in seq:
            out = self(Variable(np.array([w],dtype="int32")))
        return F.softmax(out).data
def GetLoss(model,seq):
    loss = 0.0
    for curword,nextword in zip(seq,seq[1:]):
        cur = Variable(np.array([curword],dtype="int32"))
        nxt = Variable(np.array([nextword],dtype="int32"))
        loss += model(cur,nxt).data
    return loss
    
def TrainLangModel(sequences,epoch_cnt=10):
    rnn = RNNLangModel()
    model = L.Classifier(rnn)
    model.compute_accuracy = False
    optimizer=optimizers.SGD(lr=1.)
    optimizer.setup(model)
    
    def compute_loss(seq):
        loss=0
        for curword,nextword in zip(seq,seq[1:]):
            cur = Variable(np.array([curword],dtype="int32"))
            nxt = Variable(np.array([nextword],dtype="int32"))
            loss += model(cur,nxt)
        return loss
    
    i = 0
    for epoch in xrange(epoch_cnt):
        print "Epoch %d/%d" % (epoch+1,epoch_cnt)
        for seq in sequences:
            i+=1
            #print "Sequence %d/%d" % (i,total)
            rnn.reset_state()
            seqlen = len(seq)
            model.zerograds()
            loss = model(Variable(seq[:seqlen-1]),Variable(seq[1:]))
            loss.backward()
            optimizer.update()
        
    return model

def ViterbiInference(DNN,TransMatrix,features):
    T= np.size(features,axis=0)
    prev = np.zeros((T,nb_classes))
    posteriors = F.softmax(DNN(Variable(features))).data

    pstate = posteriors[0,:]
    
    for t in range(1,T):
        prod = TransMatrix*(np.dot(pstate[:,None],posteriors[t,:][None,:]))
        pstate = np.max(prod,axis=0)
        prev[t,:] = np.argmax(prod,axis=0)
        pstate = pstate/np.sum(pstate)
    path=np.zeros(T)
    path[T-1] = np.argmax(pstate)
    for i in range(T-2,-1,-1):
        path[i] = prev[i+1,path[i+1]]
    
    return path

def BeamSearch(RNN,DNN,features):
    w = 10
    eps = 0.0001
    q = PriorQueue()
    q.push((0.0,[],object()))
    T = np.size(features,axis=0)
    for i in xrange(T):
        #print "time: %d/%d" % ((i+1),T)
        feature = features[i,None,:]
        f_likelihood = F.softmax(DNN(Variable(feature))).data[0,:]
        qq = PriorQueue(w)
        for z in xrange(25):
            maxval = -np.inf
            for like,seq,rnn in q.Iterator():
                pfeature = f_likelihood[z]
                #pseq = RNN.ActivateSeq(seq)[0,z] if len(seq)>0 else 1
                #pseq = F.softmax(RNN(Variable(np.array(seq,dtype="int32")))).data[0,z] if len(seq)>0 else 1
                if len(seq)>0:           
                    pseq = F.softmax(rnn(Variable(np.array([seq[-1]],dtype="int32")))).data[0,z]        
                else:
                    pseq = 1
                    rnn = RNN
                p = pfeature*pseq
                if p<=0.0:
                    p=eps
                val = like + np.log(p)
                if maxval < val:
                    maxval = val
                    l=val
                    s=list(seq)
                    s.append(z)
                    net = rnn.copy()
            qq.push((l,s,net))
        q = qq
    
    return q.pop()
    
class PriorQueue:
    def __init__(self,w=np.inf):
        self.list = []
        self.itemcnt = 0
        self.w = w
    def isempty(self):
        return self.itemcnt==0
    def _change(self,i,j):
        temp = self.list[i]
        self.list[i]=self.list[j]
        self.list[j]=temp
        
    def push(self,item):
        self.list.append(item)
        self.itemcnt += 1
        pos = self.itemcnt-1
        while pos>0:
            if self.list[pos][0]<self.list[pos/2][0]:
                break
            self._change(pos,pos/2)
            pos=pos/2
        if self.itemcnt > self.w:
            self.list.pop()
            self.itemcnt -= 1
    def pop(self):
        if self.isempty():
            return None
        item = self.list[0]
        self.itemcnt -= 1
        if self.itemcnt==0:
            self.list.pop()
            return item
            
        self.list[0] = self.list.pop()
        pos = 0
        while (2*pos+1)<(self.itemcnt):
            if (self.list[pos][0]<self.list[pos*2+1][0]):
                self._change(pos,pos*2+1)
                pos = pos*2+1
                continue
            elif ((pos*2+2)<self.itemcnt)and(self.list[pos][0]<self.list[pos*2+2][0]):
                self._change(pos,pos*2+2)
                pos = pos*2+2
                continue
            break
        return item
    def Iterator(self):
        for item in self.list:
            yield item