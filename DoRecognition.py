# -*- coding: utf-8 -*-
"""
Created on Thu Mar  3 13:59:30 2016

@author: wuyiming
"""


import utility as util
import DNNAcousticModel as dnn
import RNNLanguageModel as rnn
import numpy as np
from librosa.core import load,cqt
from librosa.effects import harmonic
from chainer import serializers
import chainer.links as L
import sys

"""
認識用データ用意
"""
print "Loading data..."
Audio_path = sys.argv[1]
wav,sr = load(Audio_path,sr=44100)
wav_h = harmonic(wav)
spec = np.transpose(np.abs(cqt(y=wav_h,sr=sr,hop_length=1024,bins_per_octave=24,n_bins=168,real=False,filter_scale=2))) #コード認識アルゴリズムの入力になります
spec = np.array(spec,dtype="float32")

"""
DNN読み込み
"""
dnnmodel = dnn.DNN([L.Linear(168,120),L.Linear(120,100),L.Linear(100,50),L.Linear(50,25)])  
serializers.load_npz("dnn.model",dnnmodel)
transmat = np.load("transmat.npy")

"""
認識実行。結果はresultに。
"""
print "Recognizing..."
classifier = L.Classifier(dnnmodel)

classifier.predictor.train=False
classifier.to_cpu()

accr_history = []
dnnmodel.train=False
dnnmodel.to_cpu()

result = rnn.ViterbiInference(dnnmodel,transmat,spec)

print "result:"
print result
