# -*- coding: utf-8 -*-
"""
Created on Tue Nov 17 15:59:07 2015

@author: xming
"""


import numpy as np


from chainer import Chain,ChainList,Variable,optimizers,cuda
import chainer.functions as F
import chainer.links as L
import chainer

    
def TrainDiscriminativeDNNAcousticModel(X,Y,epoch1=10,epoch2=10,gpu=False):
    xp = cuda.cupy if gpu else np
    epoch_cnt1 = epoch1
    epoch_cnt2 = epoch2
    batch_size = 100
    in_size = np.size(X,axis=1)
    datasize = np.size(X,axis=0)
    out_size = 25
    pool_size = 5
    l1 = L.Maxout(in_size,150,10)
    l2 = L.Maxout(150,120,10)
    l3 = L.Maxout(120,100,10)
    l4 = L.Linear(100,out_size)
        
    optimizer = optimizers.AdaDelta()
    
    def _performepoch(epochcnt):
        for epoch in xrange(epochcnt):
            print "epoch %d / %d" % (epoch+1,epochcnt)
            indexes = np.random.permutation(datasize)
            sum_loss=0.0
            sum_accr=0.0
            for i in range(0,datasize,batch_size):
                x = Variable(xp.asarray(X[indexes[i:i+batch_size]]))
                t = Variable(xp.asarray(Y[indexes[i:i+batch_size]]))
                optimizer.update(model,x,t)
                sum_loss += model.loss.data*batch_size
                sum_accr += model.accuracy.data*batch_size
            print "loss: %.3f" % (sum_loss/datasize)
            print "accr: %.3f" % (sum_accr/datasize)
    print "Layer 1:"            
    dnn1 = DNN([l1,L.Linear(150,out_size)],pool_size)
    model = L.Classifier(dnn1)
    if gpu:
        model.to_gpu(0)
    optimizer.setup(model)
    optimizer.add_hook(chainer.optimizer.WeightDecay(0.0001))
    _performepoch(epoch_cnt1)
    print "Layer 2:"
    dnn2 = DNN([dnn1[0].copy(),l2,L.Linear(120,out_size)],pool_size)
    model = DNNClassifier(dnn2)
    if gpu:    
        model.to_gpu(0)
    optimizer.setup(model)
    optimizer.add_hook(chainer.optimizer.WeightDecay(0.0001))
    _performepoch(epoch_cnt1)
    print "Layer 3:"
    dnn3 = DNN([dnn2[0].copy(),dnn2[1].copy(),l3,l4],pool_size)
    #dnn3.train = False
    model = DNNClassifier(dnn3)
    if gpu:    
        model.to_gpu(0)
    optimizer.setup(model)
    optimizer.add_hook(chainer.optimizer.WeightDecay(0.0001))
    _performepoch(epoch_cnt2)
    
    return dnn3.copy()
    
def TrainSimpleDNNModel(X,Y,epochcnt=10,gpu=False):
    xp = cuda.cupy if gpu else np
    epoch_cnt1 = epochcnt
    batch_size = 100
    in_size = np.size(X,axis=1)
    datasize = np.size(X,axis=0)
    out_size = 25        
    optimizer = optimizers.AdaDelta()
    
    dnn1 = DNN([L.Maxout(in_size,1000,10),L.Maxout(1000,1000,10),L.Maxout(1000,500,10),L.Linear(500,out_size)])
    model = L.Classifier(dnn1)
    if gpu:
        model.to_gpu(0)
    optimizer.setup(model)    
    for epoch in xrange(epoch_cnt1):
        print "epoch %d:" % (epoch+1)
        indexes = np.random.permutation(datasize)
        #if epoch > 10:
        #    dnn1.train=False
        #indexes = np.arange(datasize)
        sum_loss = 0.0
        sum_accr = 0.0
        for i in range(0,datasize,batch_size):
            x = Variable(xp.asarray(X[indexes[i:i+batch_size]]))
            t = Variable(xp.asarray(Y[indexes[i:i+batch_size]]))
            optimizer.update(model,x,t)
            sum_loss += model.loss.data*batch_size
            sum_accr += model.accuracy.data*batch_size
        print "loss: %.3f" % (sum_loss/datasize)
        print "accr: %.3f" % (sum_accr/datasize)
    return dnn1.copy()
    
def TrainSingleLayerNNClassifier(X,Y,epochcnt=10):
    epoch_cnt1 = epochcnt
    batch_size = 100
    in_size = np.size(X,axis=1)
    datasize = np.size(X,axis=0)
    out_size = 25        
    optimizer = optimizers.SGD()
    
    mlp = MLP(in_size,out_size)
    model = DNNClassifier(mlp)
    optimizer.setup(model)    
    for epoch in xrange(epoch_cnt1):
        print "epoch %d:" % epoch
        #indexes = np.random.permutation(datasize)
        indexes = np.arange(datasize)
        sum_loss = 0.0
        sum_accr = 0.0
        for i in range(0,datasize,batch_size):
            x = Variable(X[indexes[i:i+batch_size]])
            t = Variable(Y[indexes[i:i+batch_size]])
            model.zerograds()
            loss = model(x,t)
            loss.backward()
            optimizer.update()
            sum_loss += loss.data*batch_size
            sum_accr += model.accuracy.data*batch_size
        print "loss: %3f" % (sum_loss/datasize)
        print "accr: %3f" % (sum_accr/datasize)
    return model
    
class MLP(Chain):
    def __init__(self,in_size,out_size):
        super(MLP,self).__init__(l1=L.Linear(in_size,100),l2=L.Linear(100,out_size))
    def __call__(self,x):
        h1 = F.tanh(self.l1(x))
        h2 = F.tanh(self.l2(h1))
        return h2
        
class DNN(ChainList):
    def __init__(self,links,poolsize=5):
            
        super(DNN,self).__init__(*links)
        self.train = True
	self.poolsize = poolsize
    def __call__(self,x):
        #var = [Variable(x.data) for i in xrange(self.links_cnt)]
        h = x    
        links = self.children()
        for i in xrange(self.__len__()-1):
            li = links.next()
            h = F.dropout(li(h),ratio=.3,train=self.train)
        y = links.next()(h)
        return y
        
        
class DNNClassifier(Chain):
    def __init__(self,predictor):
        super(DNNClassifier, self).__init__(predictor=predictor)
    def SetPredictor(self,pred):
        self.predictor=pred
        
    def __call__(self,x,t):
        y = self.predictor(x)
        self.loss = F.softmax_cross_entropy(y,t)
        self.accuracy = F.accuracy(y,t)
        return self.loss
        
    def Evaluate(self,X,Y,batchsize=100):
        datasize = len(Y)
        accr = 0.0
        for i in range(0,datasize,batchsize):
            x = Variable(X[i:i+batchsize])
            t = Variable(Y[i:i+batchsize])
            self(x,t)
            accr += self.accuracy.data*batchsize
        return accr/datasize
