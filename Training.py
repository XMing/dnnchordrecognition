# -*- coding: utf-8 -*-
"""
Created on Tue Nov 17 20:18:52 2015

@author: xming
"""

import utility as util
import DNNAcousticModel as dnn
import RNNLanguageModel as rnn
import numpy as np


#audio_path="Beatles_Data/Audio/"
label_path="Beatles_Data/chordlab/The Beatles/"
cqt_path = "Beatles_Data/CQT/"



#audiolist = util.GetFileList(audio_path)
labellist = util.GetFileList(label_path)
cqtlist = util.GetFileList(cqt_path)

#print "Training Language Model..."
#train_sequences = util.ConstructLangTrainData(labellist[20:])
#langmodel = rnn.TrainLangModel(train_sequences,epoch_cnt=200)

print "Parsing Data..."
X,Y = util.ConstructTrainData(cqtlist[20:],labellist[20:])
#X = util.Lowpass(X)
transmat = util.GetTransMatrix(Y)
print "Start Training..."

dnnmodel = dnn.TrainDiscriminativeDNNAcousticModel(X,Y,epoch1=20,epoch2=20,gpu=True)
#dnnmodel = dnn.TrainSimpleDNNModel(X,Y,epochcnt = 20,gpu=True)
"""
X_feat = model.GetLastFeature(X)
print "Start training classifier..."
model_classify = dnn.TrainSimpleDNNModel(X_feat,Y,100)
print "Classifier training finished!"
"""
print "Training normal MLP..."
#classifier_mlp = dnn.TrainSingleLayerNNClassifier(X,Y,100)
print "Finished!"

Xtest,Ytest = util.ConstructTrainData(cqtlist[:20],labellist[:20])
Xtestlist,Ytestlist = util.ConstructDataPairs(cqtlist[:20],labellist[:20])
framecnt = np.size(Xtest,axis=0)

overlapcnt = 0


classifier = dnn.DNNClassifier(dnnmodel)
#classifier_deeper = dnn.DNNClassifier(model_classify.copy())

classifier.predictor.train=False
classifier.to_cpu()
#langmodel.predictor.train=False
#classifier_deeper.predictor.train=False

print "Discriminative DNN Classification score:"
print "%.3f%%" % (classifier.Evaluate(Xtest,Ytest)*100)

"""
print "Evaluation score(deeper):"
print classifier_deeper.Evaluate(Xtest_feat,Ytest)

"""
print "Normal MLP evaluation score:"
#print "%.3f%%" % (classifier_mlp.Evaluate(Xtest,Ytest)*100)


print "Evaluating hybryd structure:"
accr_history = []
dnnmodel.train=False
dnnmodel.to_cpu()
for i in range(len(Xtestlist)):
    T = float(len(Ytestlist[i]))
    print "Evaluating song %d" % (i+1)
    #langmodel.predictor.reset_state()
    #resu = rnn.BeamSearch(langmodel.predictor,dnnmodel,Xtestlist[i])
    resu = rnn.ViterbiInference(dnnmodel,transmat,Xtestlist[i])
    legal_idx = np.where(Ytestlist[i]>=0)    
    temp = np.array(Ytestlist[i][legal_idx])-np.array(resu[legal_idx])
    o = len(temp[temp==0])
    overlapcnt += o
    accr = o*100.0/T
    print "Hybrid: %.3f %%" % accr
    accr_history.append(accr)
    #print "MLP: %.3f %%" % (classifier_mlp.Evaluate(Xtestlist[i],Ytestlist[i])*100)

print "Overall accuracy: %.3f %%" % (overlapcnt * 100.0 / framecnt)
