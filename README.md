#  DNN+HMM Chord Recognizer #

DNN+HMMのコード進行認識アルゴリズムです。RNNも書いてますがあんまり使えないです。

##　動作環境 ##
* Ubuntu推奨。Windowsでは多分依存パッケージが使えません。
* Python 2.7.3+
* 依存パッケージ：　numpy , librosa , chainer , ffmpeg(mp3ファイルを読む場合のみ)。ffmpeg以外はpipでインストールできます。

## 認識の流れ ##
* 入力された音声はパーカッション音を分離し、Constant-Q変換してスペクトラム系列を得ます。
* DNN（深層ニューラルネットワーク）は、各フレームのスペクトラムから各ラベルの生成確率分布を計算します。
* DNNの出力を各時刻においてのHMMの出力確率分布とみなし、Viterbi探索で最尤系列を求めます。

## 使い方 ##
* DoRecognition.pyに認識プログラムを書いてみました。以下の命令で走らせることができます。
```
#!shell

python DoRecognition.py 音楽ファイル名
```

*  DoLearning.pyは認識に必要なdnn.modelとtransmatrix.npyを生成するものです。生成にはモデルを訓練するための音楽データとラベルデータが必要です。
*  認識できるコードはメジャー、マイナートライアド各１２種とno-chordの、計25種類です。0~11がCMaj,C#Maj,D#Maj...BMajで、12~23が同min、24がno-chordです。
*  認識結果はフレーム単位のラベル系列です。各フレームの間隔は1024サンプルで、SRが44100Hzなのでだいたい0.03秒くらいです。